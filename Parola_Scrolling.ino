// Use the Parola library to scroll text on the display
//
// Demonstrates the use of the scrolling function to display text received 
// from the serial interface
//
// User can enter text on the serial monitor and this will display as a
// scrolling message on the display.
// Speed for the display is controlled by a pot on SPEED_IN analog in.
// Scrolling direction is controlled by a switch on DIRECTION_SET digital in.
// Invert ON/OFF is set by a switch on INVERT_SET digital in.
//
// Keyswitch library can be found at https://github.com/MajicDesigns/MD_KeySwitch
//
// NOTE: MD_MAX72xx library must be installed and configured for the LED
// matrix type being used. Refer documentation included in the MD_MAX72xx 
// library or see this link: 
// https://majicdesigns.github.io/MD_MAX72XX/page_hardware.html
//

#include <MD_Parola.h>
#include <MD_MAX72xx.h>
#include <SPI.h>
#include <SoftwareSerial.h>

//#define HARDWARE_TYPE MD_MAX72XX::GENERIC_HW

// set to 1 if we are implementing the user interface pot, switch, etc
#define	USE_UI_CONTROL	0

#if USE_UI_CONTROL
#include <MD_KeySwitch.h>
#endif

// Turn on debug statements to the serial output
#define  DEBUG  0

#if  DEBUG
#define	PRINT(s, x)	{ Serial.print(F(s)); Serial.print(x); }
#define	PRINTS(x)	Serial.print(F(x))
#define	PRINTX(x)	Serial.println(x, HEX)
#else
#define	PRINT(s, x)
#define PRINTS(x)
#define PRINTX(x)
#endif

// Define the number of devices we have in the chain and the hardware interface
// NOTE: These pin numbers will probably not work with your hardware and may 
// need to be adapted
#define	MAX_DEVICES	4
#define	CLK_PIN		13
#define	DATA_PIN	11
#define	CS_PIN		10

// HARDWARE SPI
MD_Parola P = MD_Parola(CS_PIN, MAX_DEVICES);
// SOFTWARE SPI
//MD_Parola P = MD_Parola(DATA_PIN, CLK_PIN, CS_PIN, MAX_DEVICES);
boolean sendonce=true;
#define	PAUSE_TIME		1000
#define	SPEED_DEADBAND	5

// Scrolling parameters
#if USE_UI_CONTROL
#define	SPEED_IN		A5
#define	DIRECTION_SET	8	// change the effect
#define	INVERT_SET		9	// change the invert

#endif // USE_UI_CONTROL

uint8_t	frameDelay = 30;	// default frame delay value
textEffect_t	scrollEffect = PA_SCROLL_LEFT;

// Global message buffers shared by Serial and Scrolling functions
#define	BUF_SIZE	161
char curMessage[BUF_SIZE];
char newMessage[BUF_SIZE];
bool newMessageAvailable = false;

void readSerial(void)
{
/**
  while (Serial.available())
  {
    newMessage[putIndex] = (char)Serial.read();
    if ((newMessage[putIndex] == '\n') || (putIndex >= BUF_SIZE-2))	// end of message character or full buffer
    {
      // put in a message separator and end the string
      newMessage[putIndex] = '\0';
      // restart the index for next filling spree and flag we have a message waiting
      putIndex = 0;
      newMessageAvailable = true;
    }
      else
      // Just save the next char in next location
      newMessage[putIndex++];
  }**/
  strcpy(newMessage, "Hello there (testing)!");
  newMessageAvailable = true;
}

void setup()
{
  P.begin();
  P.displayClear();
  P.displaySuspend(false);

  P.displayScroll(curMessage, PA_LEFT, scrollEffect, frameDelay);
 
  strcpy(curMessage, "Hello! Enter new message?");
  newMessage[0] = '\0';
}

void loop() 
{
  readSerial();
  if (P.displayAnimate()) 
  {
    if (newMessageAvailable)
    {
      strcpy(curMessage, newMessage);
      newMessageAvailable = false;
    }
    P.displayReset();
  }
}
